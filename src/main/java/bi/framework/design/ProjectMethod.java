package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import bl.framework.api.SeleniumBase;
import util.ReadExcel;

public class ProjectMethod extends SeleniumBase{
	@BeforeMethod(groups= {"common"})
	public void login() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
	}

	@DataProvider(name="fetchData")
	public Object[][] getData(){
		return ReadExcel.readData(dataSheetName);
	}
}
