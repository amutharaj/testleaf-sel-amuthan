package util;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReportProject {

	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	@Test
	
	public void runReport() {
		html=new ExtentHtmlReporter("./Report/extentReporter.html");
		extent = new ExtentReports();
		
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test=extent.createTest("TC001_Login","Login into TetLeaf");
		test.assignAuthor("Amuthan");
		test.assignCategory("Smoke Testing");
		test.pass("User Entered Successfully");
		
		
		try {
			test.fail("User Entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		extent.flush();
		
		}
	
	
}
